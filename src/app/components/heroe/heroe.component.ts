import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HeroesService } from '../../servicios/heroes.service';

@Component({
  selector: 'app-heroe',
  templateUrl: './heroe.component.html',
  styles: [
  ]
})
export class HeroeComponent implements OnInit {

  heroe: any = {};

  constructor( private activatedRoute: ActivatedRoute, 
               private servicioHeroes: HeroesService) { 
    activatedRoute.params.subscribe( params => {
      console.log(params['id']);
      this.heroe = this.servicioHeroes.getHeroe(params['id']);
      console.log(this.heroe);
    });
  }

  ngOnInit(): void {
  }



}
